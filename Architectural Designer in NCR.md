Since SDAARCHITECT's inception in the 1990s, it has quickly emerged as one of the most recognized Delhi based architectural firms, establishing a reputation for delivering imaginative architectural design in the area of institutional architecture. Seeking to reorder architectural priorities by putting people first, SDAARCHITECT challenges conventional perspectives of museum design that will, over time, influence the core typology of modern institutional architecture in India. Through a range of architectural and planning projects, SDAARCHITECT seeks to reconcile the conditions and complexities of an Indian context with the design elegance of contemporary architecture, at the same time not losing sight of the the humanistic aspect of architectural design - qualities with benefit and nurture us as human beings: the desire for protected shelter, seamless transitions between indoor and outdoor spaces and the general improvement of quality of life through good design.
Architect in NCR
Architectural Designer in NCR
Sunando Dasgupta and Associates is a leading Architecture and Interior Design firm based in Delhi, India. An industry leader in the design of museum architecture, SDAARCHITECT has an extensive portfolio, ranging from commercial architecture to large scale institutional and planning.

https://sdaarchitect.net

https://sdaarchitect.business.site/

https://goo.gl/maps/16TqJKh8xZ5bogBx8

https://www.facebook.com/sdaarchitect

https://twitter.com/sdaarchitect

https://www.instagram.com/sdaarchitect

https://in.pinterest.com/delhi_architect2/

https://www.linkedin.com/in/sdaarchitect/

https://www.youtube.com/channel/UC25IGXE1-OSp72b7JGdZ80A

https://www.youtube.com/watch?v=1M-N59DlkY8

https://medium.com/@delhi_architect2
